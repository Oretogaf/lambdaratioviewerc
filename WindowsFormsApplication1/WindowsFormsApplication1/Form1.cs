﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            getPorts();
        }

        private void getPorts()
        {
            String[] ports = SerialPort.GetPortNames();
            portSelected.Items.AddRange(ports);
        }

        private void portSelected_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("El puerto ya estaba cerrado");
            }

            try
            {
                serialPort1.PortName = portSelected.Text;
                serialPort1.BaudRate = 9600;
                serialPort1.Open();

                ReadData read = new ReadData(serialPort1);
                Program.readData = new Thread(read.readSerialData);
                Program.readData.Start();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("El puerto selecinado no esta disponible");
            }
        }

        public void showSerialData(double lambda)
        {
            labdaRatio.Text = lambda.ToString();
            if (lambda <= 0.99)
            {
                mixtureType.Text = "Rich";
            }
            else if (lambda <= 1.01)
            {
                mixtureType.Text = "Stoichiometric";
            }
            else
            {
                mixtureType.Text = "Lean";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.readData.Abort();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.readData.Abort();
        }
    }
}
