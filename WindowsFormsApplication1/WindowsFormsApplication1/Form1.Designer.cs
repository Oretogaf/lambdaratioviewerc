﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.title = new System.Windows.Forms.Label();
            this.labdaRatio = new System.Windows.Forms.Label();
            this.mixtureType = new System.Windows.Forms.Label();
            this.portSelected = new System.Windows.Forms.ComboBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.copy = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.title.Location = new System.Drawing.Point(95, 54);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(110, 20);
            this.title.TabIndex = 0;
            this.title.Text = "Lambda Ratio";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labdaRatio
            // 
            this.labdaRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labdaRatio.Location = new System.Drawing.Point(105, 87);
            this.labdaRatio.Name = "labdaRatio";
            this.labdaRatio.Size = new System.Drawing.Size(90, 40);
            this.labdaRatio.TabIndex = 1;
            this.labdaRatio.Text = "-";
            this.labdaRatio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mixtureType
            // 
            this.mixtureType.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mixtureType.Location = new System.Drawing.Point(30, 127);
            this.mixtureType.Name = "mixtureType";
            this.mixtureType.Size = new System.Drawing.Size(240, 40);
            this.mixtureType.TabIndex = 2;
            this.mixtureType.Text = "-";
            this.mixtureType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // portSelected
            // 
            this.portSelected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portSelected.FormattingEnabled = true;
            this.portSelected.Location = new System.Drawing.Point(90, 210);
            this.portSelected.Name = "portSelected";
            this.portSelected.Size = new System.Drawing.Size(120, 21);
            this.portSelected.TabIndex = 3;
            this.portSelected.SelectedIndexChanged += new System.EventHandler(this.portSelected_SelectedIndexChanged);
            // 
            // copy
            // 
            this.copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copy.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.copy.Location = new System.Drawing.Point(110, 236);
            this.copy.Name = "copy";
            this.copy.Size = new System.Drawing.Size(80, 15);
            this.copy.TabIndex = 4;
            this.copy.Text = "GUILLEN®";
            this.copy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.copy);
            this.Controls.Add(this.portSelected);
            this.Controls.Add(this.mixtureType);
            this.Controls.Add(this.labdaRatio);
            this.Controls.Add(this.title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Lambda Ratio Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label labdaRatio;
        private System.Windows.Forms.Label mixtureType;
        private System.Windows.Forms.ComboBox portSelected;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label copy;
    }
}

