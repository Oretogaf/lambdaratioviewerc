﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class ReadData
    {
        public SerialPort serialPort1;

        public ReadData(SerialPort serialPort)
        {
            serialPort1 = serialPort;
        }

        public void readSerialData()
        {
            while (true)
            {
                try
                {
                    if (serialPort1.ReadLine().Trim() != "")
                    {
                        double lambda = ((Convert.ToDouble(serialPort1.ReadLine()) * (0.68 / 1023)) + 0.68);
                        lambda = Math.Round(lambda, 2);
                        Console.WriteLine(lambda);
                        Program.form.Invoke((MethodInvoker)delegate { Program.form.showSerialData(lambda); });
                    }
                }
                catch (IOException)
                {
                    Console.WriteLine("Se produjo un error al leer, el dispositivo se ha desconectado");
                    serialPort1.Close();
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Se produjo un error al leer, operacion no permitida");
                    serialPort1.Close();
                }
                catch (TimeoutException)
                {
                    Console.WriteLine("Se produjo un error al leer, timeout");
                    serialPort1.Close();
                }
                catch (FormatException)
                {
                    Console.WriteLine("Se produjo un error al leer, formato incorrecto");
                }
            }
        }
    }
}
